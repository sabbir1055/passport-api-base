<?php

namespace App\Http\Controllers\Auth;
use App\User;
use Auth;
use GuzzleHttp\Client;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
      /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
      */

      use AuthenticatesUsers;

      /**
      * Where to redirect users after login.
      *
      * @var string
      */
      protected $redirectTo = '/home';

      /**
      * Create a new controller instance.
      *
      * @return void
      */
      public function __construct()
      {
            $this->middleware('guest')->except('logout');
      }

      public function Login(LoginRequest $request){
            $http = new Client;
            $response = $http->post(url('oauth/token'), [
                  'form_params' => [
                        'grant_type' => 'password',
                        'client_id' => '2',
                        'client_secret' => 'end5TPVE7ksAVMhHG2ODM6bUwfAWj0iqPF63EcJr',
                        'username' => $request->email,
                        'password' => $request->password,
                        'scope' => '',
                  ],
            ]);
            $credentials    = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                  $data = auth()->user();
                  $user =  [
                              'id'        => $data->id,
                              'name'      => $data->getUserData ? $data->getUserData->name : '',
                              'image'     => $data->getUserData ? $data->getUserData->image : '',
                              'email'     => $data->email
                        ];
                  return response(['auth'=>json_decode((string) $response->getBody(), true),'user'=>$user]);
            }else{
                  return response()->json([
                        'message' => 'Invalid email or password'
                  ]);
            }
      }
      public function Register(RegisterRequest $request){
            $user=new User();
            $user->email      = $request->email;
            $user->password   = bcrypt($request->password);
            $user->save();
            $http = new Client;
            $response = $http->post(url('oauth/token'), [
                  'form_params' => [
                        'grant_type' => 'password',
                        'client_id' => '2',
                        'client_secret' => 'end5TPVE7ksAVMhHG2ODM6bUwfAWj0iqPF63EcJr',
                        'username' => $request->email,
                        'password' => $request->password,
                        'scope' => '',
                  ],
            ]);
            $credentials    = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                  $data = auth()->user();
                  $user =  [
                              'id'        => $data->id,
                              'name'      => $data->getUserData ? $data->getUserData->name : '',
                              'image'     => $data->getUserData ? $data->getUserData->image : '',
                              'email'     => $data->email
                        ];
                  return response(['auth'=>json_decode((string) $response->getBody(), true),'user'=>$user]);
            }else{
                  return response()->json([
                        'message' => 'Invalid email or password'
                  ]);
            }
      }
}
